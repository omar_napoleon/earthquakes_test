/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class GeoJsonSummary implements Serializable{

    private String type;

    private MetaData metadata;

    private List<Object> features = new ArrayList<>();

    private List<Object> bbox = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MetaData getMetadata() {
        return metadata;
    }

    public void setMetadata(MetaData metadata) {
        this.metadata = metadata;
    }

    public List<Object> getFeatures() {
        return features;
    }

    public void setFeatures(List<Object> features) {
        this.features = features;
    }

    public List<Object> getBbox() {
        return bbox;
    }

    public void setBbox(List<Object> bbox) {
        this.bbox = bbox;
    }
    
    

}
