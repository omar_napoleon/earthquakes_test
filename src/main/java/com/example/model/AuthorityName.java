package com.example.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}