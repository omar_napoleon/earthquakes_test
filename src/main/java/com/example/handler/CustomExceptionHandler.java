/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.handler;

import com.example.model.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author Huenei
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{

    /**
     * Provides handling for standard Spring MVC exceptions.
     *
     * @param e the target exception
     * @param request the current request
     * @return
     */
    @ExceptionHandler({IllegalArgumentException.class,
        EmptyResultDataAccessException.class,
        MethodArgumentTypeMismatchException.class,
        Exception.class})
    public final ResponseEntity<Object> handleAllExceptions(Exception e, WebRequest request) throws JsonProcessingException {
        Message message = new Message();
        message.setMessage(e.getMessage());
        HttpStatus status = HttpStatus.ACCEPTED;
        ObjectMapper objectMapper = new ObjectMapper();

        if (e instanceof IllegalArgumentException) {
            status = HttpStatus.NOT_FOUND;
        } else if (e instanceof EmptyResultDataAccessException) {
            status = HttpStatus.NOT_FOUND;
            message.setMessage("Not found");
        } else if (e instanceof MethodArgumentTypeMismatchException) {
            status = HttpStatus.BAD_REQUEST;
         } else if (e instanceof InvalidFormatException) {
             message.setMessage("Invalid data body");
            status = HttpStatus.BAD_REQUEST;
        } else if (e instanceof Exception) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity(objectMapper.writeValueAsString(message), status);
    }
}
