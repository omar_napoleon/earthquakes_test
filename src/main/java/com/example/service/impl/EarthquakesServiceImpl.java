/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service.impl;

import com.example.model.GeoJsonSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.service.client.ClientService;

/**
 *
 * @author usuario
 */
@Service
public class EarthquakesServiceImpl {

    @Autowired
    private ClientService clientService;

    /**
     *
     * @param date1
     * @param date2
     * @return
     * @throws Exception
     */
    public GeoJsonSummary getEarthquakesByDates(String date1, String date2) throws Exception {

        return clientService.getEarthquakesByDates(date1, date2);

    }
    
    /**
     *
     * @param mag1
     * @param mag2
     * @return
     * @throws Exception
     */
    public GeoJsonSummary getEarthquakesByMagnitudes(String mag1, String mag2) throws Exception {

        return clientService.getEarthquakesByMagnitudes(mag1, mag2);

    }
}
