/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service.client;

import com.example.configuration.UrlConfiguration;
import com.example.model.GeoJsonSummary;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Huenei
 */
@Service
public class ClientService {

    @Autowired
    private UrlConfiguration urlConfiguration;

    /**
     *
     * @param date1
     * @param date2
     * @return
     * @throws java.lang.Exception
     */
    public GeoJsonSummary getEarthquakesByDates(String date1, String date2) throws Exception {

        GeoJsonSummary result = callMicroService(new StringBuilder(
                urlConfiguration.getEarthquakes())
                .append("starttime=").append(date1)
                .append("&endtime=").append(date2).toString());

        return result;
    }
    
    
    /**
     *
     * @param mag1
     * @param mag2
     * @return
     * @throws java.lang.Exception
     */
    public GeoJsonSummary getEarthquakesByMagnitudes(String mag1, String mag2) throws Exception {

        GeoJsonSummary result = callMicroService(new StringBuilder(
                urlConfiguration.getEarthquakes())
                .append("minmagnitude=").append(mag1)
                .append("&maxmagnitude=").append(mag2).toString());

        return result;
    }

    /**
     *
     * @param url
     * @param header
     * @param value
     * @return
     * @throws java.lang.Exception
     */
    private GeoJsonSummary callMicroService(String url) throws Exception {

        // HttpHeaders
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
        // Request to return JSON format
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Data attached to the request.
        HttpEntity<GeoJsonSummary> entity = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters()
                .add(new MappingJackson2HttpMessageConverter());

        // Send request with POST method.
        ResponseEntity<GeoJsonSummary> result = restTemplate.exchange(url, HttpMethod.GET,
                entity, GeoJsonSummary.class);

        return result.getBody();
    }

}
