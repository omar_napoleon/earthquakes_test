/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Huenei
 */
@Configuration
@ConfigurationProperties("url")
public class UrlConfiguration {

    private String protocol;
    private String host;
    private String port;
    private String earthquakes;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getEarthquakes() {
        return earthquakes;
    }

    public void setEarthquakes(String earthquakes) {
        this.earthquakes = earthquakes;
    }

    
}
