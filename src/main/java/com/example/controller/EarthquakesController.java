/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

import com.example.model.GeoJsonSummary;
import com.example.request.JwtAuthenticationRequest;
import com.example.security.JwtTokenUtil;
import com.example.service.impl.EarthquakesServiceImpl;
import com.example.service.impl.JwtUserDetailsServiceImpl;
import com.restaurant.service.JwtAuthenticationResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author usuario
 */
@Api(value = "earthquakes API REST full")
@RestController
public class EarthquakesController {

    @Autowired
    private EarthquakesServiceImpl earthquakesServiceImpl;

    /**
     *
     * @param startTime
     * @param endTime
     * @return
     * @throws java.lang.Exception
     */
    @ApiOperation(value = "Busca la información de sismos entre un rango de fechas", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Recuperada la información  exitosamente", response = Object.class)
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @GetMapping(value = "/earthquakes/dates", produces = {"application/json"})
    public ResponseEntity<GeoJsonSummary> getEarthquakesByDates(
            @ApiParam(value = "fecha de inicio", required = true)
            @Valid @RequestParam(value = "starttime", required = true)
            @DateTimeFormat(pattern = "dd-MM-yyyy") @NotBlank String startTime,
            @ApiParam(value = "fecha fin", required = true)
            @Valid @RequestParam(value = "endtime", required = true)
            @DateTimeFormat(pattern = "dd-MM-yyyy") @NotBlank String endTime) throws Exception {

        return new ResponseEntity<>(earthquakesServiceImpl.
                getEarthquakesByDates(startTime, endTime), HttpStatus.OK);
    }
    
    /**
     *
     * @param mag1
     * @param mag2
     * @return
     * @throws java.lang.Exception
     */
    @ApiOperation(value = "Busca la información de sismos entre un rango de magnitudes", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Recuperada la información  exitosamente", response = Object.class)
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @GetMapping(value = "/earthquakes/magnitudes", produces = {"application/json"})
    public ResponseEntity<GeoJsonSummary> getEarthquakesByMagnitudes(
            @ApiParam(value = "magnitud minima", required = true)
            @Valid @RequestParam(value = "minmagnitude", required = true)
            @DecimalMin(value = "0.0", inclusive = false) @Digits(integer=1, fraction=1)
            BigDecimal mag1,
            @ApiParam(value = "magnitud máxima", required = true)
            @Valid @RequestParam(value = "maxmagnitude", required = true)
            @DecimalMin(value = "0.0", inclusive = false) @Digits(integer=1, fraction=1)
            BigDecimal mag2) throws Exception {
        

        return new ResponseEntity<>(earthquakesServiceImpl.
                getEarthquakesByMagnitudes(mag1.toString(), mag2.toString()), HttpStatus.OK);

    }

}
