/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

import com.example.configuration.WebSecurityConfig;
import com.example.request.JwtAuthenticationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author usuario
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Import(WebSecurityConfig.class)
public class EarthquakesControllerTest {

    @Autowired
    private MockMvc mvc;


    @After
    public void tearDown() {
    }

    @Test
    public void testGetEarthquakesByMagnitudes() throws Exception {

        System.out.println("getEarthquakesByMagnitudes");
        BigDecimal mag1 = new BigDecimal("5.6");
        BigDecimal mag2 = new BigDecimal("7");
        String accessToken = obtainAccessToken("user", "password");
        mvc.perform(MockMvcRequestBuilders
                .get("/earthquakes/magnitudes")
                .param("minmagnitude", mag1.toString())
                .param("maxmagnitude", mag2.toString())
                .header("Authorization", accessToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String obtainAccessToken(String username, String password) throws Exception {

        JwtAuthenticationRequest authenticationRequest = new JwtAuthenticationRequest();
        authenticationRequest.setUsername(username);
        authenticationRequest.setPassword(password);
        ObjectMapper mapper = new ObjectMapper();

        ResultActions result
                = mvc.perform(MockMvcRequestBuilders
                        .post("/auth")
                        .content(mapper.writeValueAsString(authenticationRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());

        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("token").toString();
    }

//    /**
//     * Test of getEarthquakesByDates method, of class EarthquakesController.
//     */
//    @Test
//    public void testGetEarthquakesByDates() throws Exception {
//        ObjectMapper mapper = new ObjectMapper();
//        System.out.println("getEarthquakesByDates");
//        String startTime = "2019-10-13";
//        String endTime = "2019-10-14";
//        EarthquakesController instance = new EarthquakesController();
//        ResponseEntity<GeoJsonSummary> expResult = 
//                new ResponseEntity<>(mapper.readValue(json, GeoJsonSummary.class), HttpStatus.OK);
//        ResponseEntity<GeoJsonSummary> result = instance.getEarthquakesByDates(startTime, endTime);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getEarthquakesByMagnitudes method, of class EarthquakesController.
//     */
//    @Test
//    public void testGetEarthquakesByMagnitudes() throws Exception {
//        ObjectMapper mapper = new ObjectMapper();
//        System.out.println("getEarthquakesByMagnitudes");
//        BigDecimal mag1 = new BigDecimal("5.6");
//        BigDecimal mag2 = new BigDecimal("7");
//        EarthquakesController instance = new EarthquakesController();
//        ResponseEntity<GeoJsonSummary> expResult = 
//                new ResponseEntity<>(mapper.readValue(json, GeoJsonSummary.class), HttpStatus.OK);
//        ResponseEntity<GeoJsonSummary> result = instance.getEarthquakesByMagnitudes(mag1, mag2);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
