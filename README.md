#_API REST  Earthquakes
Se exponen los metodo de la API REST FULL. se uso como lenguaje de desarrollo Java con el framework Spring Boot y como base de datos H2(EL script de la estructura y los datos se encuentra dentro del código fuente en una archivo llamado data.sql). Al levantar el proyecto localmente, se crea y se inicializa la bd con datos de prueba automáticamene.

# SWAGGER (Documentación de la API)
La API cuenta con Swagger como medio de documentación relacionada al funcionamiento y descripción de los métodos expuestos por la API, es decir, muestra todo lo necesario para poder consumir dicha API.
Para acceder al swagger de la API, se debe desplegar el micro servicio de forma local. Luego abrir una ventana con el navegador de su preferencia con la siguiente URL:
•http://localhost:8080/swagger-ui.html#

## Métodos expuestos

## Login (POST)
Método de tipo POST que autoriza  las consultas a través de la generación de un token de seguridad que debe añadirse en los header de los métodos a consultar
### url
```
http://localhost:8080/auth
``` 
### body
```
{
    "username": "user",
    "password": "password"
}
``` 
### Ejemplo parámetros del header que deben añadirse
```
Content-Type:application/json
Authorization:eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTY0MzM5NTYwNzAyLCJleHAiOjE1NjQ5NDQzNjB9.r4wKHk_Kpd63QxquK44r_DPzTA4lMLbVYeqXJyCGwD4wu0tqAFXR51vvIJzejORxwpIxTSZLguvzO8twhQV81A
``` 

## Consultar por fechas(GET)
Método de tipo GET que dado un rango de fechas 
### url
```
http://localhost:9091/earthquakes/dates?starttime=2019-10-13&endtime=2019-10-14
``` 

## Consultar por magnitudes(GET)
Método de tipo GET que dado un rango de magnitudes 
### url
```
http://localhost:8080/earthquakes/magnitudes?minmagnitude=6.555&maxmagnitude=7
``` 

.